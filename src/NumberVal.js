import React, { Component } from 'react';

class NumberVal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: props.numberVal
    };
    console.log('Constructor');
  }

  componentWillMount() {
    console.log('Component will mount');
  }

  componentDidMount() {
    console.log('Component did mount');
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.numberVal !== nextProps.numberVal){
      console.log('Component will receive props', nextProps);
      this.setState({number: nextProps.numberVal});
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('Should component update: ', nextProps, nextState);
    return true; 
  }

  componentWillUpdate(nextProps, nextState) {
    console.log('Component will update', nextProps, nextState);
    alert('The component is gonna be update to ' + nextProps.numberVal); // if 'shouldComponentUpdate' return false, the alert won't show
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('Component did update', prevProps, prevState);
    setTimeout(()=> {
      alert("It's updated");
    }, 3000);
  }
  
  componentWillUnmount(){
    console.log('Component will unmount');
  }

  render() {
    return (
      <div>
        <div>{this.state.number}</div>
      </div>
    );
  }
}

export default NumberVal;
