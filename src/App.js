import React, { Component } from 'react';
import NumberVal from './NumberVal';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: 0
    };
  }
  updateNumber = () => this.setState({ number: this.state.number + 1 });

  render() {
    return (
      <div className="App">
        <NumberVal numberVal={this.state.number} />
        <button onClick={this.updateNumber}>Update Number</button>
      </div>
    );
  }
}

export default App;
